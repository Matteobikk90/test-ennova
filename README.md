## Ennova Test

The requirements of the test are to display some products based on a API call.

On long press to a single card the user must be able to see the ingredients of that item, otherwise if the user clicks to it he should be able to add the item to a shopping cart that will shows the properties of it(name, quantity, price) plus the total ammount.

A "azzera" button will reset the bill.

## Screenshots

Please check the requirements.pdf in the root folder for a better idea of the project

## Tech/framework used

ES6
npx create-react-app
npm node-sass

npm eslint-config-airbnb. In conflict with create-react-app I had to run 2 different projects and try to merge the code into the main one

React.js
SCSS
GIT
Flexbox

Tested with Chrome, Firefox, Safari, Opera

## Difficulties

Run into difficulties at the end, when adding multiple quantities of the same product to the bill.
Also when pushing the first clicked item inside the array cart.

Conflict with eslint airbnb and create-react-app

## How to run?

Please clone this repo and run 'npm install' to install node_modules into the app, then run it by 'npm start'
