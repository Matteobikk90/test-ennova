import React from "react";
import ProductGrid from "./components/ProductsGrid/ProductsGrid";
import "./App.scss";

function App() {
  return (
    <main>
      <ProductGrid />
    </main>
  );
}

export default App;
