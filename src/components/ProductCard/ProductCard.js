import React, { Fragment, useState, useEffect } from "react";
import ShoppingCart from "../ShoppingCart/ShoppingCart";

function ProductCard() {
  const [products, setProducts] = useState([]);
  const [shoppingCart, setShoppingCart] = useState([]);
  const [total, setTotal] = useState(0);
  let timer;

  const fetchProducts = async () => {
    const data = await fetch(
      "https://my-json-server.typicode.com/marcopaoletti/db_junior_frontend/products"
    );
    const products = await data.json();
    setProducts(products);
  };

  const handleShoppingCart = product => {
    product.quantity = 1;
    const sameProduct = shoppingCart.includes(product);
    if (sameProduct) {
      product.quantity++;
    } else {
      setShoppingCart([...shoppingCart, product]);
    }
    shoppingCart.reduce((prev, curr) => {
      const price = curr.price;
      setTotal(prev + price);
      return prev + price;
    }, 0);
    return shoppingCart;
  };

  const handleButtonPress = e => {
    e.persist();
    setTimeout(() => e.target.classList.toggle("active"), 1500);
  };

  const handleButtonRelease = () => {
    clearTimeout(timer);
  };

  const handleReset = () => {
    setShoppingCart([]);
    setTotal(0);
  };

  const handleCloseIngredients = e => {
    e.currentTarget.parentElement.parentElement.parentElement.children[0].classList.remove(
      "active"
    );
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  const ProductCardComponent = products.map((product, i) => {
    const { name, img, price, backgroundColor, ingredients } = product;
    return (
      <div
        key={i}
        id={i}
        className="card"
        onTouchStart={handleButtonPress}
        onTouchEnd={handleButtonRelease}
        onMouseDown={handleButtonPress}
        onMouseUp={handleButtonRelease}
        onMouseLeave={handleButtonRelease}
        onClick={() => handleShoppingCart(product)}
      >
        {img !== null ? (
          <div
            className="cardInnerImg"
            style={{ backgroundImage: `url("${img}")` }}
          />
        ) : (
          <div
            className="cardInnerColor"
            style={{ background: backgroundColor }}
          />
        )}
        <div className="opening">
          <div className="header">
            <h2>{name}</h2>
            <button onClick={handleCloseIngredients} type="button">
              x
            </button>
            {img !== null ? (
              <div
                className="cardInnerImg"
                style={{ backgroundImage: `url("${img}")` }}
              />
            ) : (
              <div
                className="cardInnerColor"
                style={{ background: backgroundColor }}
              />
            )}
          </div>
          <div className="content">
            <p>Ingredienti</p>
            <ul>
              {ingredients.map((ingredient, j) => {
                return <li key={j}>{ingredient}</li>;
              })}
            </ul>
          </div>
        </div>
        <div className="cardInnerDetails">
          <h1>{name}</h1>
          <p>&euro; {price}</p>
        </div>
      </div>
    );
  });

  return (
    <>
      {ProductCardComponent}
      <ShoppingCart
        handleReset={handleReset}
        items={shoppingCart}
        total={total}
      />
    </>
  );
}

export default ProductCard;
