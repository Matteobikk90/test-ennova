import React from "react";
import ProductCard from "../ProductCard/ProductCard";

function ProductsGrid() {
  return (
    <div className="grid">
      <ProductCard />
    </div>
  );
}

export default ProductsGrid;
