import React from "react";

function ShoppingCart(props) {
  let totalQuantity = 0;

  const shoppingCartComponent = props.items.map((item, i) => {
    const { name, price, quantity } = item;
    totalQuantity = props.items.length;
    return (
      <div key={i} className="shoppingCartItem">
        <div>
          <span>{quantity}</span>
          <span> X {name}</span>
        </div>
        <div>
          <span>&euro; </span>
          <span>{price * quantity}</span>
        </div>
      </div>
    );
  });

  return (
    <div className="shoppingCart">
      {shoppingCartComponent}
      <div className="shoppingCartTotal">
        <div>
          <input type="text" name="pz" value={totalQuantity} readOnly />
          <span>pz.</span>
        </div>
        <div>
          <span>tot</span>
          <input type="text" name="total" value={props.total} readOnly />
        </div>
      </div>
      <button className="cartButton" onClick={props.handleReset} type="button">
        azzera
      </button>
    </div>
  );
}

export default ShoppingCart;
